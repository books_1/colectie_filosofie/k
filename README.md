# K

## Content

```
./Karl Jaspers:
Karl Jaspers - Oamenii de insemnatate cruciala.pdf

./Karl Raimund Popper:
Karl Raimund Popper - Conjecturi si infirmari.pdf
Karl Raimund Popper - Cunoasterea si problema raportului corp-minte.pdf
Karl Raimund Popper - Filosofie sociala si filosofia stiintei.pdf
Karl Raimund Popper - In cautarea unei lumi mai bune.pdf
Karl Raimund Popper - Lectia acestui secol.pdf
Karl Raimund Popper - Logica cercetarii.pdf
Karl Raimund Popper - Mitul contextului.pdf
Karl Raimund Popper - Mizeria istoricismului.pdf
Karl Raimund Popper - Societatea deschisa si dusmanii ei, vol. 1.pdf

./Karl Raimund Popper & Konrad Lorentz:
Karl Raimund Popper & Konrad Lorentz - Viitorul este deschis.pdf
```

